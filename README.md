<p align="center"><a href="https://laravel.com" target="_blank"><img src="https://raw.githubusercontent.com/laravel/art/master/logo-lockup/5%20SVG/2%20CMYK/1%20Full%20Color/laravel-logolockup-cmyk-red.svg" width="400" alt="Laravel Logo"></a></p>

<!-- <p align="center">
<a href="https://github.com/laravel/framework/actions"><img src="https://github.com/laravel/framework/workflows/tests/badge.svg" alt="Build Status"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/dt/laravel/framework" alt="Total Downloads"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/v/laravel/framework" alt="Latest Stable Version"></a>
<a href="https://packagist.org/packages/laravel/framework"><img src="https://img.shields.io/packagist/l/laravel/framework" alt="License"></a>
</p> -->

# **Fileshare | Comparte archivos con tus usuarios**

---

Comparte archivos con los usuarios de tu grupo, obten feedback y sube revisiones de este

---

Esta realizado con:

-   Lenguage: **PHP**
-   Diseño: **Blade**
-   Frameworks: **Laravel**
-   Librerias: **Livewire v3**, **AlpineJS v3**, **TailwindCSS v3**, **alajusticia/Laravel-expirable v2**, **Laravel jetstream v4**, **Blade icons v1.5 (Blade forkawesome v1.9)**, **Quant core vdev-main**, **rappasoft/Laravel-livewire-tables vdev-v3-develop**, **spatie/laravel-permission v5**, **usamamuneerchaudhary/Commentify v1.3**
-   Librerias dev: **barryvdh/laravel-ide-helper v2.1**, **fakerphp/faker v1.2**, **friendsofphp/php-cs-fixer v3.2**, **laravel/pint v1.1**, **laravel/sail v1.2**, **mockery/mockery v1.6**, **nunomaduro/collision v7.8**, **pestphp/pest v2.1**, **pestphp/pest-plugin-laravel v2.2**, **spatie/laravel-ignition v2.3**

---

Características de la versión:

-   Sistema básico de login, registro y usuarios (gracias a Jetstream)
-   Permisos y roles
-   Dashboard y CRUDs
-   Menu plegable

---

## **Imágenes**

### Login
![Versión actual](images/login.png)
### Dashboard
![Versión actual](images/dashboard.png)
### Menu plegable
![Versión actual](images/menu_collapsible.gif)
### Users
![Versión actual](images/users.png)
### Companies
![Versión actual](images/companies.png)
### Roles
![Versión actual](images/roles.png)
### Permissions
![Versión actual](images/permissions.png)

---

## Para ejecutar se debe:

1. Clonar el repositorio
2. Tener instalado [PHP v8.1](https://www.php.net), [composer v2.6](https://getcomposer.org), [Node v18.17 (npm v9.6)](https://nodejs.org)
3. Configura las credenciales de la base de datos
4. Abrir la terminal en la carpeta del repositorio
5. Ejecutar el siguiente comando para crear tablas y usuario principal
```
php artisan migrate:fresh --seed
```
6. Ejecutar el siguiente comando
```
php artisan serve
```
7. Abrir: http://localhost en su navegador
8. Conectarse con las credenciales: `superadmin@example.com`:`password`

---

Fileshare | Comparte archivos con tus usuarios

Comparte archivos con los usuarios de tu grupo, obten feedback y sube revisiones de este

Author: @BlackBlex (BlackBlex)

Licencia: [**MIT**](https://opensource.org/licenses/MIT)
