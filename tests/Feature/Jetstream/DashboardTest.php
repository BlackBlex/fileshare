<?php

use function Pest\Laravel\{get};

it('has dashboard page', function () {
    $response = get('/dashboard');

    $response->assertStatus(200);
});
