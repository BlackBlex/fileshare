<?php

use App\Models\Permission;
use App\Models\Role;
use App\Models\User;
use Illuminate\Support\Collection;

beforeEach(function () {
    $this->roles = [
        'admin' => [
            'view',
            'create',
            'update',
            'delete',
        ],
        'editor' => [
            'view',
            'create',
            'update'
        ],
        'member' => [
            'view'
        ],
        'super-admin' => [],
    ];
});

test('Roles creation', function () {
    collect($this->roles)->each(function ($permissions, $role) {
        if (!Role::where('name', $role)->exists()) {
            $role = Role::create(['name' => $role, 'team_id' => null]);
        }
    });

    /** @var Pest\Expectation expect */
    $expect = expect(Role::get());
    $expect->toBeInstanceOf(Collection::class)->toHaveCount(4);
    //expect()->
});

test('Permission creation', function () {
    collect($this->roles)->each(function ($permissions, $roleName) {
        $role = Role::findByName($roleName);

        collect($permissions)->each(function ($permission) use ($role) {
            $permi = Permission::findOrCreate($permission);

            if (!$role->hasPermissionTo($permission)) {
                /** @var Permission */
                $role->permissions()->save($permi);
            }
        });
    });

    $permissions = Permission::get();

    /** @var Pest\Expectation expect */
    $expect = expect($permissions);
    $expect->toBeInstanceOf(Collection::class)->toHaveCount(4);
});

test('Assign role to user', function () {
    if (!User::where('name', 'Jovani')->exists()) {
        User::factory()->withPersonalTeam()->create([
            'name' => 'Jovani',
        ]);
    }

    if (!User::where('name', 'Roberto Mitt')->exists()) {
        User::factory()->withPersonalTeam()->create([
            'name' => 'Roberto Mitt',
        ]);
    }

    $user = User::where('name', 'Jovani')->first();
    $userOther = User::where('name', 'Roberto Mitt')->first();

    //Set current team id
    $user->currentTeam;
    expect($user->current_team_id)->toBeInt()->toBeGreaterThan(0);

    $userOther->currentTeam;
    expect($userOther->current_team_id)->toBeInt()->toBeGreaterThan(0);

    //Prepair to set roles
    if (!$user->hasRole('super-admin')) {
        $user->assignRole('super-admin');
    }

    //Prepair to set roles
    //With team
    setPermissionsTeamId($userOther->current_team_id);
    if (!$userOther->hasRole('member')) {
        $userOther->assignRole('member');
    }

    expect($user->hasRole('super-admin'))->toBeTrue();
    expect($userOther->hasRole('member'))->toBeTrue();
});

test('Verify current role from user Jovani', function () {
    $jovaniUser = User::where('name', 'Jovani')->first();

    $jovaniUser->currentTeam;

    /** @var Pest\Expectation expect */
    $expect = expect($jovaniUser);
    $expect->not()->toBeNull();

    $roleFromJovani = Role::user($jovaniUser)->get()->pluck('name');

    expect($roleFromJovani->toArray())->toContain('super-admin');
});
