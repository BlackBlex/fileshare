<?php

use App\Livewire\Commentify\Comment as LivewireComment;
use App\Models\Commentify\Comment;
use App\Models\User;
use Livewire\Livewire;

test('Commentify comment override', function () {
    $comment = Comment::factory()->create();

    expect($comment)->toBeInstanceOf(Comment::class);
});

test('Commentify its user work', function () {
    $user = User::factory()->create();

    $comment = Comment::factory()->create([
        'user_id' => $user->id,
    ]);

    expect($comment->user)->toBeInstanceOf(User::class);
});

test('Commentify can mention', function () {
    $user = User::factory()->create([
        'name' => 'Jovani',
    ]);

    $userOther = User::factory()->create([
        'name' => 'Roberto Mitt',
    ]);

    $comment = Comment::factory()->create([
        'body' => 'Test mentions',
        'user_id' => $user->id,
    ]);

    $commentOther = Comment::factory()->create([
        'parent_id' => $comment->id,
    ]);

    $this->actingAs($user);
    $component = Livewire::test(LivewireComment::class, ['comment' => $commentOther])
        ->set('isReplying', true)
        ->assertDontSee('@Roberto_Mitt')
        ->assertDontSee('@robertomitt')
        ->set('replyState.body', '@roberto')
        ->call('getUsers', 'roberto')
        ->assertSee('Roberto')
        ->call('selectUser', 'roberto');
    $this->assertEquals('@roberto ', $component->get('replyState.body'));
});

test('Commentify load children successful', function () {
    /** @var TestCase $this */

    $user = User::factory()->create([
        'name' => 'Jovani',
    ]);

    $userOther = User::factory()->create([
        'name' => 'Roberto Mitt',
    ]);

    $comment = Comment::factory()->create([
        'body' => 'Test children',
        'user_id' => $user->id,
    ]);

    $commentOther = Comment::factory()->create([
        'parent_id' => $comment->id,
        'body' => 'I\'m child',
    ]);

    expect($comment->children)->toBeInstanceOf(Illuminate\Database\Eloquent\Collection::class);
    expect($commentOther->parent_id)->toBeInt()->toBeGreaterThan(0);
});

test('Commentify children have user', function () {
    /** @var TestCase $this */

    $user = User::factory()->create([
        'name' => 'Jovani',
    ]);

    $userOther = User::factory()->create([
        'name' => 'Roberto Mitt',
    ]);

    $comment = Comment::factory()->create([
        'body' => 'Test children',
        'user_id' => $user->id,
    ]);

    $commentOther = Comment::factory()->create([
        'parent_id' => $comment->id,
        'body' => 'I\'m child',
    ]);

    expect($comment->children)->toBeInstanceOf(Illuminate\Database\Eloquent\Collection::class);
    expect($comment->children[0]->user)->not->toBeNull();
    //expect($comment->children()[0]->user)->toBeInstanceOf(\Usamamuneerchaudhary\Commentify\Models\User::class);
});
