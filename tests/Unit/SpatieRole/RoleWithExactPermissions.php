<?php

use App\Models\Role;

test('Get member role with exact permissions', function () {
    $permissionsID = [1];

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('member');
});

test('Get editor role with exact permissions', function () {
    $permissionsID = [3, 1, 2];

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('editor');
});

test('Get another role with exact permissions', function () {
    $permissionsID = [3, 2, 4];

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('another');
});

test('Get admin role with exact permissions', function () {
    $permissionsID = [4, 2, 1, 3];

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(1);
    expect($roles[0])->toBe('admin');
});

test('No get role with exact permissions', function () {
    $permissionsID = [4, 1, 3];

    $roles = Role::exactPermission($permissionsID)->get()->pluck('name');

    expect($roles->toArray())->toBeArray()->toHaveCount(0);
});
