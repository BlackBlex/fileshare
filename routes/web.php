<?php

use App\Http\Controllers\AdminPageController;
use App\Http\Controllers\FileController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\UserPageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', [UserPageController::class, 'index'])->name('home_index');

Route::middleware([
    'auth',
    config('jetstream.auth_session'),
    'verified',
])->group(function () {
    Route::get('/dashboard', [UserPageController::class, 'dashboard'])->name('dashboard')->middleware(['role:member|editor']);

    Route::name('admin.')->prefix('admin')->group(function () {
        Route::middleware(['role:super-admin|admin'])->group(function () {
            Route::get('', [AdminPageController::class, 'index']);
            Route::get('dashboard', [AdminPageController::class, 'dashboard'])->name('dashboard');
            Route::get('users', [AdminPageController::class, 'users'])->name('users');
            Route::get('companies', [AdminPageController::class, 'companies'])->name('companies');
            Route::resource('users', UserController::class)->only('create', 'show', 'edit', 'destroy');
        });

        Route::get('/permissions', [AdminPageController::class, 'permissions'])->name('permissions')->middleware(['role:super-admin']);
    });

    // Route::get('/permissions', [RolesAndPermissionsController::class, 'index'])->name('showAssignedRoles');
    // Route::post('/assign_role', [RolesAndPermissionsController::class, 'store'])->name('assignRole');
    // Route::delete('/revoke_role', [RolesAndPermissionsController::class, 'destroy'])->name('revokeRole');

    Route::resource('files', FileController::class)->only('index', 'create', 'show', 'edit', 'destroy');
});
