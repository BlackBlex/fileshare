<?php

namespace Database\Factories;

use App\Models\File;
use Illuminate\Database\Eloquent\Factories\Factory;

/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\File>
 */
class FileFactory extends Factory
{
    protected $model = File::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'code' => $this->faker->regexify('[A-Z0-9]{12}'),
            'is_deleted' => $this->faker->boolean(),
            'status' => $this->faker->randomElement(['Initial', 'Reviewed', 'Updated', 'Authorized']),
            'description' => $this->faker->text(),
            'filename' => $this->faker->words(3, true) . '.' . $this->faker->fileExtension(),
            'downloads' => $this->faker->numberBetween(0, 3000),
        ];
    }
}
