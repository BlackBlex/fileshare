<?php

namespace Database\Factories\Commentify;

use App\Models\Commentify\Comment;
use App\Models\User;
use Usamamuneerchaudhary\Commentify\Database\Factories\CommentFactory as BaseCommentFactory;

class CommentFactory extends BaseCommentFactory
{
    protected $model = Comment::class;

    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition(): array
    {
        return [
            'body' => fake()->text,
            'user_id' => function () {
                return User::factory()->create()->id;
            },
            'parent_id' => null,
            'commentable_type' => '\ArticleStub',
            'commentable_id' => 1,
            'created_at' => now()
        ];
    }
}
