<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('files', function (Blueprint $table) {
            $table->id();
            $table->string('code', 12)->unique();
            $table->foreignId('folder_id')->constrained("folders");
            $table->foreignId('uploaded_by')->constrained("users");
            $table->string('filename')->unique();
            $table->string('description');
            $table->bigInteger('downloads')->default(0);
            $table->enum('status', ['Initial', 'Reviewed', 'Updated', 'Authorized'])->default('Initial');
            $table->boolean('is_deleted')->default(false);

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('files');
    }
};
