<?php

namespace Database\Seeders;

use App\Models\Subscription;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSuperAdminSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $user = User::factory()->create([
            'name' => 'Super Admin',
            'email' => 'superadmin@example.com',
        ]);

        if ($user != null) {
            if ($user->ownedTeams->isNotEmpty()) {
                $user->currentTeam;
            }

            Subscription::create([
                'user_id' => $user->id,
            ]);
        }
    }
}
