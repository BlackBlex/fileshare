<?php

namespace Database\Seeders;

use App\Models\File;
use App\Models\Folder;
use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class FileSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $folders = Folder::get();
        $users = User::get();

        foreach ($users as $user) {
            foreach ($folders as $folder) {
                File::factory()->count(2)->create([
                    'uploaded_by' => $user->id,
                    'folder_id' => $folder->id,
                ]);
            }
        }
    }
}
