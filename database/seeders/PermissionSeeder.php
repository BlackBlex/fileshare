<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //Some initially role configuration
        $roles = [
            'admin' => [
                'view',
                'create',
                'update',
                'delete',
            ],
            'editor' => [
                'view',
                'create',
                'update'
            ],
            'member' => [
                'view'
            ],
            'super-admin' => [],
        ];

        collect($roles)->each(function ($permissions, $role) {
            $role = Role::create(['name' => $role, config('permission.column_names.team_foreign_key') => null]);

            collect($permissions)->each(function ($permission) use ($role) {
                /** @var Permission */
                $permi = Permission::findOrCreate($permission);
                $role->permissions()->save($permi);
            });
        });

        $users = User::get()->shuffle();

        $admin_taken = false;
        /** @var bool $admin_taken */
        /** @param User $user */
        collect($users)->each(function ($user) use ($admin_taken) {
            if ($user->current_team_id != null) {
                setPermissionsTeamId($user->current_team_id);
            }

            if (!$admin_taken) {
                $user->assignRole('super-admin');
                $admin_taken = true;
            } else {
                $user->assignRole('member');
            }
        });
    }
}
