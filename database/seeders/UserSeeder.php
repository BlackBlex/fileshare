<?php

namespace Database\Seeders;

use App\Models\Subscription;
use App\Models\Team;
use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        $users = User::factory()->withPersonalTeam()->count(5)->create();

        foreach ($users as $user) {
            $user->currentTeam;

            Subscription::create([
                'user_id' => $user->id,
            ]);
        }
    }
}
