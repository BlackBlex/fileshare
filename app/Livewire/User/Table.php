<?php

namespace App\Livewire\User;

use App\Models\Role;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ImageColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;

class Table extends DataTableComponent
{
    protected $model = User::class;

    public $columnSearch = [
        'name' => null,
        'email' => null,
        'team' => null,
        'role' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id')
        ->setSearchDebounce(250)
        ->setColumnSelectDisabled()
        ->setTableWrapperAttributes([
            'default' => false,
            'class' => 'shadow border-b border-gray-200 dark:border-gray-700 overflow-hidden',
        ])
        ->setSearchFieldAttributes([
            'class' => 'input input-bordered',
        ])
        ->setTableRowUrl(function ($row) {
            return route('admin.users.show', $row);
        })
        ->setTableRowUrlTarget(function ($row) {
            return '_blank';
        });
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id')
                ->sortable(),
            ImageColumn::make('Avatar')
                ->location(function ($row) {
                    return asset($row->profile_photo_url);
                })
                ->attributes(function ($row) {
                    return [
                        'class' => 'w-8 h-8 rounded-full',
                    ];
                }),
            Column::make('Name', 'name')
                ->sortable()
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhere('users.name', 'like', '%' . $searchTerm . '%'))
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'name', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Email', 'email')
                ->sortable()
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhere('users.email', 'like', '%' . $searchTerm . '%'))
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'email', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Company', 'current_team_id')
                ->sortable()
                ->searchable(
                    fn (Builder $query, $searchTerm) => $query->orWhereHas('currentTeam', function (Builder $query2) use ($searchTerm) {
                        $query2->Where('teams.name', 'like', '%' . $searchTerm . '%');
                    })
                )
                ->format(
                    fn (int|null $teamId, User $user, Column $column) => $user->currentTeam->name ?? 'No team'
                )
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'team', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Roles')
                ->sortable()
                ->searchable(fn (Builder $query, $searchTerm) => $query->orWhereHas('roles_', function (Builder $query2) use ($searchTerm) {
                    $query2->where('roles.name', 'like', '%' . $searchTerm . '%');
                }))
                ->label(function (User $user, Column $row) {
                    $user->currentTeam;
                    $teamId = $user->current_team_id;
                    $roles = Role::user($user, $teamId)->get();

                    return $roles->pluck('name')->implode(', ');
                })
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'role', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Active until')
                ->sortable()
                ->label(function (User $user, Column $row) {
                    return $user->subscription->expires_at->toDateString();
                }),
            // ComponentColumn::make('Test', 'remember_token')
            //     ->component('checkbox')
            //     ->attributes(fn (string $value, User $user, Column $column) => [
            //         'id' => 'check' . $user->name,
            //         'checked' => str_starts_with($user->name, 'A'),
            //     ]),
            // Column::make('Created at', 'created_at')
            //     ->sortable()
            //     ->format(
            //         fn (Carbon $date, User $user, Column $column) => $date->toDateString()
            //     ),
            Column::make('Updated at', 'updated_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, User $user, Column $column) => $date->toDateString()
                ),
            ButtonGroupColumn::make('Actions')
                ->unclickable()
                ->attributes(function ($row) {
                    return [
                        'class' => 'space-x-2',
                    ];
                })
                ->buttons([
                    // LinkColumn::make('View')
                    //     ->title(fn ($row) => '<x-forkawesome-eye />')
                    //     ->location(fn ($row) => route('admin.users.show', $row))
                    //     ->attributes(function ($row) {
                    //         return [
                    //             'target' => '_blank',
                    //             'class' => 'px-3 py-1.5 text-xs font-bold text-gray-700 dark:text-gray-300 uppercase transition-all duration-150 ease-linear bg-pink-400 rounded
                    //                 shadow outline-none active:bg-pink-500 focus:outline-none hover:bg-pink-600 hover:shadow-inner hover:shadow-pink-800',
                    //         ];
                    //     }),
                    LinkColumn::make('Edit')
                    ->title(fn ($row) => 'Edit')
                    ->location(fn ($row) => route('admin.users.edit', $row))
                    ->attributes(function ($row) {
                        return [
                            'class' => 'px-3 py-1.5 text-xs font-bold text-gray-700 dark:text-gray-300 uppercase transition-all duration-150 ease-linear bg-blue-400 rounded
                                shadow outline-none active:bg-blue-500 focus:outline-none hover:bg-blue-600 hover:shadow-inner hover:shadow-blue-800',
                        ];
                    }),
                    LinkColumn::make('Delete')
                        ->title(fn ($row) => 'Delete')
                        ->location(fn ($row) => route('admin.users.destroy', $row))
                        ->attributes(function ($row) {
                            return [
                                'class' => 'px-3 py-1.5 text-xs font-bold text-gray-700 uppercase transition-all duration-150 ease-linear bg-red-500 rounded
                                shadow outline-none active:bg-red-600 focus:outline-none hover:bg-red-700 hover:shadow-inner hover:shadow-red-900 dark:text-gray-300',
                            ];
                        }),
                ]),
        ];
    }

    public function builder(): Builder
    {
        $globalQuery = User::query()

        ->when($this->columnSearch['name'] ?? null, fn (Builder $query, string $name) => $query->where('users.name', 'like', '%' . $name . '%'))
        ->when($this->columnSearch['email'] ?? null, fn (Builder $query, string $email) => $query->where('users.email', 'like', '%' . $email . '%'))
        ->when($this->columnSearch['team'] ?? null, fn (Builder $query, string $team) => $query->whereHas('currentTeam', function (Builder $query2) use ($team) {
            $query2->where('teams.name', 'like', '%' . $team . '%');
        }))
        ->when($this->columnSearch['role'] ?? null, fn (Builder $query, string $role) => $query->whereHas('roles_', function (Builder $query2) use ($role) {
            $query2->where('roles.name', 'like', '%' . $role . '%');
        }));

        return $globalQuery;
    }
}
