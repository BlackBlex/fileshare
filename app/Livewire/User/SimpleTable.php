<?php

namespace App\Livewire\User;

use App\Models\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class SimpleTable extends DataTableComponent
{
    protected $model = User::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id')
        ->setReorderDisabled()
        ->setPaginationDisabled()
        ->setSearchDisabled()
        ->setColumnSelectDisabled()
        ->setPaginationVisibilityDisabled()
        ->setComponentWrapperAttributes([
            'class' => 'bg-gray-200 dark:bg-gray-700 pb-4',
        ])
        ->setTableWrapperAttributes([
            'default' => false,
            'class' => 'shadow border-b border-gray-200 dark:border-gray-700 overflow-hidden',
        ])
        ->setTableAttributes(['default' => false, 'class' => 'table-fixed divide-y divide-gray-200 dark:divide-none'])
        ->setThAttributes(function (Column $column) {
            return [
                'default' => false,
                'class' => 'px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:bg-gray-800 dark:text-gray-400'
            ];
        })
        ->setTdAttributes(function (Column $column, $row, $columnIndex, $rowIndex) {
            return [
                'default' => false,
                'class' => 'px-6 py-4 text-sm font-medium dark:text-white'
            ];
        });
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id'),
            Column::make('Name', 'name'),
            Column::make('Email', 'email'),
            Column::make('Created at', 'created_at')
                ->format(
                    fn (Carbon $date, User $user, Column $column) => $date->toDateString()
                ),
            Column::make('Updated at', 'updated_at')
                ->format(
                    fn (Carbon $date, User $user, Column $column) => $date->toDateString()
                ),
        ];
    }

    public function builder(): Builder
    {
        return User::query()->orderBy('id', 'desc')->limit(5);
    }
}
