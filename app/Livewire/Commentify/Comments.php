<?php

namespace App\Livewire\Commentify;

use  Usamamuneerchaudhary\Commentify\Http\Livewire\Comments as BaseComments;
use Illuminate\Contracts\View\Factory;
use Illuminate\Contracts\View\View;
use Illuminate\Foundation\Application;

class Comments extends BaseComments
{
    /**
     * @return Factory|Application|View|\Illuminate\Contracts\Foundation\Application|null
     */
    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application|null
    {
        $comments = $this->model
            ->comments()
            ->with('user', 'children.user', 'children.children')
            ->parent()
            ->latest()
            ->paginate(config('commentify.pagination_count', 10));
        return view('livewire.commentify.comments', [
            'comments' => $comments
        ]);
    }
}
