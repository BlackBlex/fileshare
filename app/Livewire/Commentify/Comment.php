<?php

namespace App\Livewire\Commentify;

use  Usamamuneerchaudhary\Commentify\Http\Livewire\Comment as BaseComment;
use App\Models\User;
use Livewire\Attributes\On;

class Comment extends BaseComment
{
    /**
     * @return Factory|Application|View|\Illuminate\Contracts\Foundation\Application|null
     */
    public function render(): \Illuminate\Contracts\View\Factory|\Illuminate\Foundation\Application|\Illuminate\Contracts\View\View|\Illuminate\Contracts\Foundation\Application|null
    {
        return view('livewire.commentify.comment');
    }

    /**
     * @param $searchTerm
     * @return void
     */
    #[On('getUsers')]
    public function getUsers($searchTerm): void
    {
        if (!empty($searchTerm)) {
            $this->users = User::where('name', 'like', '%' . $searchTerm . '%')->take(5)->get();
        } else {
            $this->users = [];
        }
    }
}
