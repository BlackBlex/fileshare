<?php

namespace App\Livewire;

use Livewire\Component;

class HeaderMenu extends Component
{
    public function render()
    {
        return view('header-menu');
    }
}
