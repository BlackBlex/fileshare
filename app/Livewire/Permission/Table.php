<?php

namespace App\Livewire\Permission;

use App\Models\Permission;
use App\Models\Role;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Filters\MultiSelectFilter;

class Table extends DataTableComponent
{
    protected $model = Permission::class;

    public $columnSearch = [
        'name' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id')
        ->setSearchDebounce(250)
        ->setColumnSelectDisabled()
        ->setTableWrapperAttributes([
            'default' => false,
            'class' => 'shadow border-b border-gray-200 dark:border-gray-700 overflow-hidden',
        ]);
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id')
                ->sortable(),
            Column::make('Name', 'name')
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhere('permissions.name', 'like', '%' . $searchTerm . '%')),
            Column::make('Roles')
                ->label(function (Permission $permission, Column $column) {
                    $roles = $permission->roles;

                    // xdebug_break();

                    return $roles->pluck('name')->implode(', ');
                })
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhereHas('roles', function (Builder $query2) use ($searchTerm) {
                    $query2->where('roles.name', 'like', '%' . $searchTerm . '%');
                })),
            Column::make('Created at', 'created_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, Permission $permission, Column $column) => $date->toDateString()
                ),
            Column::make('Updated at', 'updated_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, Permission $permission, Column $column) => $date->toDateString()
                ),
        ];
    }

    public function filters(): array
    {
        return [
            MultiSelectFilter::make('Roles')
                ->options(
                    Role::query()
                        ->orderBy('name')
                        ->get()
                        ->keyBy('id')
                        ->map(fn ($role) => $role->name)
                        ->toArray()
                )->filter(function (Builder $builder, array $values) {
                    $builder->whereHas('roles', function (Builder $query2) use ($values) {
                        $query2->whereIn('roles.id', $values);
                    });
                }),
        ];
    }
}
