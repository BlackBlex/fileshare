<?php

namespace App\Livewire\File;

use App\Models\File;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class SimpleTable extends DataTableComponent
{
    protected $model = File::class;

    public function configure(): void
    {
        $this->setPrimaryKey('id')
        ->setReorderDisabled()
        ->setPaginationDisabled()
        ->setSearchDisabled()
        ->setColumnSelectDisabled()
        ->setPaginationVisibilityDisabled()
        ->setComponentWrapperAttributes([
            'class' => 'bg-gray-200 dark:bg-gray-700 pb-4',
        ])
        ->setTableWrapperAttributes([
            'default' => false,
            'class' => 'shadow border-b border-gray-200 dark:border-gray-700 overflow-hidden',
        ])
        ->setTableAttributes(['default' => false, 'class' => 'table-fixed divide-y divide-gray-200 dark:divide-none'])
        ->setThAttributes(function (Column $column) {
            return [
                'default' => false,
                'class' => 'px-6 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider dark:bg-gray-800 dark:text-gray-400'
            ];
        })
        ->setTdAttributes(function (Column $column, $row, $columnIndex, $rowIndex) {
            return [
                'default' => false,
                'class' => 'px-6 py-4 text-sm font-medium dark:text-white'
            ];
        });
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id'),
            Column::make('Folder', 'folder_id')
            ->format(fn ($val, $file) => $file->folder->name),
            Column::make('Filename', 'filename'),
            // Column::make('Description', 'description'),
            Column::make('Uploaded by', 'uploaded_by')
            ->format(fn ($val, $file) => $file->uploader->name),
            BooleanColumn::make('Is deleted', 'is_deleted')->yesNo(),
            Column::make('Created at', 'created_at')
                ->format(
                    fn (Carbon $date, File $file, Column $column) => $date->toDateString()
                ),
        ];
    }

    public function builder(): Builder
    {
        return File::query()->orderBy('id', 'desc')->limit(5);
    }
}
