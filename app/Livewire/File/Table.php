<?php

namespace App\Livewire\File;

use App\Models\File;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\ButtonGroupColumn;
use Rappasoft\LaravelLivewireTables\Views\Columns\LinkColumn;
use Rappasoft\LaravelLivewireTables\Views\Filters\TextFilter;

class Table extends DataTableComponent
{
    protected $model = File::class;

    public $columnSearch = [
        'filename' => null,
        'description' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id')
            ->setReorderEnabled()
            ->setSingleSortingDisabled()
            ->setHideReorderColumnUnlessReorderingEnabled()
            ->setFilterLayoutSlideDown()
            ->setRememberColumnSelectionDisabled()
            ->setSecondaryHeaderTrAttributes(function ($rows) {
                return ['class' => 'bg-gray-100'];
            })
            ->setSecondaryHeaderTdAttributes(function (Column $column, $rows) {
                if ($column->isField('id')) {
                    return ['class' => 'text-red-500'];
                }

                return ['default' => true];
            })
            ->setFooterTrAttributes(function ($rows) {
                return ['class' => 'bg-gray-100'];
            })
            ->setFooterTdAttributes(function (Column $column, $rows) {
                if ($column->isField('name')) {
                    return ['class' => 'text-green-500'];
                }

                return ['default' => true];
            })
            ->setUseHeaderAsFooterEnabled()
            ->setHideBulkActionsWhenEmptyEnabled();
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id')
                ->sortable()
                ->setSortingPillTitle('Key')
                ->setSortingPillDirections('0-9', '9-0')
                ->html(),
            Column::make('Folder', 'folder_id')
                ->sortable()
                ->format(fn (string $value, File $file) => $file->folder->name),
            Column::make('Filename', 'filename')
                ->sortable()
                ->searchable()
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'filename', 'columnSearch' => $this->columnSearch]);
                })
                ->html(),
            Column::make('Description', 'description')
                ->sortable(),
            Column::make('Uploaded by', 'uploaded_by')
                ->sortable()
                ->format(fn (string $value, File $file) => $file->uploader->name),
            BooleanColumn::make('Is deleted', 'is_deleted')
                ->sortable()
                ->yesNo(),
            Column::make('Created at', 'created_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, File $file, Column $column) => $date->toDateString()
                ),
            Column::make('Updated at', 'updated_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, File $file, Column $column) => $date->toDateString()
                ),
            ButtonGroupColumn::make('Actions')
                ->unclickable()
                ->attributes(function ($row) {
                    return [
                        'class' => 'space-x-2',
                    ];
                })
                ->buttons([
                    LinkColumn::make('View')
                        ->title(fn ($row) => 'View')
                        ->location(fn ($row) => route('files.show', $row))
                        ->attributes(function ($row) {
                            return [
                                'target' => '_blank',
                                'class' => 'px-3 py-1.5 text-xs font-bold text-gray-700 dark:text-gray-300 uppercase transition-all duration-150 ease-linear bg-pink-400 rounded
                                shadow outline-none active:bg-pink-500 focus:outline-none hover:bg-pink-600 hover:shadow-inner hover:shadow-pink-800',
                            ];
                        }),

                    LinkColumn::make('Manage')
                    ->title(fn ($row) => 'Manage' . $row->username)
                    ->location(fn ($row) => route('files.edit', $row))
                    ->attributes(function ($row) {
                        return [
                            'class' => 'px-3 py-1.5 text-xs font-bold text-gray-700 dark:text-gray-300 uppercase transition-all duration-150 ease-linear bg-blue-400 rounded
                            shadow outline-none active:bg-blue-500 focus:outline-none hover:bg-blue-600 hover:shadow-inner hover:shadow-blue-800',
                        ];
                    }),
                    LinkColumn::make('Delete')
                        ->title(fn ($row) => 'Delete ' . $row->username)
                        ->location(fn ($row) => route('files.destroy', $row))
                        ->attributes(function ($row) {
                            return [
                                'class' => 'px-3 py-1.5 text-xs font-bold text-gray-700 uppercase transition-all duration-150 ease-linear bg-red-500 rounded
                            shadow outline-none active:bg-red-600 focus:outline-none hover:bg-red-700 hover:shadow-inner hover:shadow-red-900 dark:text-gray-300',
                            ];
                        }),
                ]),
        ];
    }

    public function filters(): array
    {
        return [
            TextFilter::make('Filename')
                ->config([
                    'maxlength' => 5,
                    'placeholder' => 'Search Filename',
                ])
                ->filter(function (Builder $builder, string $value) {
                    $builder->where('files.filename', 'like', '%' . $value . '%');
                }),
        ];
    }

    public function builder(): Builder
    {
        return File::query();
    }
}
