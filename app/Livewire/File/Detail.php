<?php

namespace App\Livewire\File;

use App\Models\File;
use Livewire\Component;

class Detail extends Component
{
    public File $file;

    public function delete()
    {
        $this->file->delete();

        return redirect()->to(route("files.index"));
    }

    public function render()
    {
        return view('livewire.file.detail');
    }
}
