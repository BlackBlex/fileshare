<?php

namespace App\Livewire\Role;

use App\Models\Role;
use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;

class Table extends DataTableComponent
{
    protected $model = Role::class;

    public $columnSearch = [
        'name' => null,
        'company' => null,
        'guard' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id')
        ->setSearchDebounce(250)
        ->setColumnSelectDisabled()
        ->setTableWrapperAttributes([
            'default' => false,
            'class' => 'shadow border-b border-gray-200 dark:border-gray-700 overflow-hidden',
        ]);
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id')
                ->sortable(),
            Column::make('Name', 'name')
                ->sortable()
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhere('roles.name', 'like', '%' . $searchTerm . '%'))
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'name', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Company')
                ->sortable()
                ->label(function (Role $role, Column $row) {
                    $role = Role::where('id', $role->id)->first();

                    if (is_null($role->team_id)) {
                        return 'Global';
                    }

                    /** @var App\Models\Team expect */
                    $team = Team::where('id', $role->team_id)->first();

                    return $team->name;
                })
                ->searchable(function (Builder $query, string $searchTerm) {
                    if (str_starts_with($searchTerm, 'Glo')) {
                        $query->orWhereNull('roles.team_id');
                    } else {
                        $query->orWhereHas('team', function (Builder $query2) use ($searchTerm) {
                            $query2->where('teams.name', 'like', '%' . $searchTerm . '%');
                        });
                    }
                })
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'company', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Guard', 'guard_name')
                ->sortable()
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhere('roles.guard_name', 'like', '%' . $searchTerm . '%'))
                ->secondaryHeader(function () {
                    return view('tables.cells.input-search', ['field' => 'guard', 'columnSearch' => $this->columnSearch]);
                }),
            Column::make('Permissions')
                ->sortable()
                ->label(function (Role $role, Column $row) {
                    $permissions = $role->permissions;
                    $numPermissions = count($permissions);

                    if ($numPermissions > 0) {
                        return $numPermissions;
                    }

                    return 'All';
                }),
            Column::make('Created at', 'created_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, Role $role, Column $column) => $date->toDateString()
                ),
            Column::make('Updated at', 'updated_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, Role $role, Column $column) => $date->toDateString()
                ),
        ];
    }

    public function builder(): Builder
    {
        $globalQuery = Role::query();

        return $globalQuery
        ->when($this->columnSearch['name'] ?? null, fn (Builder $query, string $name) => $query->where('roles.name', 'like', '%' . $name . '%'))
        ->when($this->columnSearch['company'] ?? null, function (Builder $query, string $company) {
            if (str_starts_with($company, 'Glo')) {
                $query->whereNull('roles.team_id');
            } else {
                $query->whereHas('team', function (Builder $query2) use ($company) {
                    $query2->where('teams.name', 'like', '%' . $company . '%');
                });
            }
        })
        ->when($this->columnSearch['guard'] ?? null, fn (Builder $query, string $name) => $query->where('roles.guard_name', 'like', '%' . $name . '%'));
    }
}
