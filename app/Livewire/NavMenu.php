<?php

namespace App\Livewire;

use App\Models\MenuItem;
use Livewire\Component;

class NavMenu extends Component
{
    public function render()
    {
        $menu = [
            (new MenuItem())
                ->setTitle('Admin dashboard')
                ->setIcon('forkawesome-home')
                ->setUrl('admin.dashboard')
                ->setHasRoles(['super-admin', 'admin']),
            (new MenuItem())
                ->setTitle('Dashboard')
                ->setIcon('forkawesome-home')
                ->setUrl('dashboard')
                ->setHasnRoles(['super-admin', 'admin']),
            (new MenuItem())
                ->setTitle('User management')
                ->setHasRoles(['super-admin', 'admin'])
                ->setChildren([
                    (new MenuItem())
                        ->setTitle('Users')
                        ->setIcon('forkawesome-users')
                        ->setUrl('admin.users'),
                    (new MenuItem())
                        ->setTitle('Companies')
                        ->setIcon('forkawesome-social-home')
                        ->setUrl('admin.companies'),
                    (new MenuItem())
                        ->setTitle('Permissions')
                        ->setIcon('forkawesome-lock')
                        ->setUrl('admin.permissions')
                        ->setHasRoles(['super-admin']),
                ]),
        ];

        return view('nav-menu', compact('menu'));
    }
}
