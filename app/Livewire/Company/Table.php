<?php

namespace App\Livewire\Company;

use App\Models\Team;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Rappasoft\LaravelLivewireTables\DataTableComponent;
use Rappasoft\LaravelLivewireTables\Views\Column;
use Rappasoft\LaravelLivewireTables\Views\Columns\BooleanColumn;

class Table extends DataTableComponent
{
    protected $model = Team::class;

    public $columnSearch = [
        'name' => null,
    ];

    public function configure(): void
    {
        $this->setPrimaryKey('id')
        ->setSearchDebounce(250)
        ->setColumnSelectDisabled()
        ->setTableWrapperAttributes([
            'default' => false,
            'class' => 'shadow border-b border-gray-200 dark:border-gray-700 overflow-hidden',
        ]);
    }

    public function columns(): array
    {
        return [
            Column::make('Id', 'id')
                ->sortable(),
            Column::make('Name', 'name')
                ->searchable(fn (Builder $query, string $searchTerm) => $query->orWhere('teams.name', 'like', '%' . $searchTerm . '%')),
            BooleanColumn::make('Personal owner', 'personal_team')
                ->sortable()
                ->yesNo(),
            Column::make('Created at', 'created_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, Team $team, Column $column) => $date->toDateString()
                ),
            Column::make('Updated at', 'updated_at')
                ->sortable()
                ->format(
                    fn (Carbon $date, Team $team, Column $column) => $date->toDateString()
                ),
        ];
    }

    public function builder(): Builder
    {
        return Team::query()
        ->when($this->columnSearch['name'] ?? null, fn (Builder $query, string $name) => $query->where('teams.name', 'like', '%' . $name . '%'));
    }
}
