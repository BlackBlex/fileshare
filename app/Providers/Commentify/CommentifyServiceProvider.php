<?php

namespace App\Providers\Commentify;

use App\Livewire\Commentify\Comment;
use App\Livewire\Commentify\Comments;
use App\Livewire\Commentify\Like;
use App\Models\Commentify\Comment as CommentifyComment;
use Illuminate\Support\Facades\Gate;
use Livewire\Livewire;
use Usamamuneerchaudhary\Commentify\Policies\CommentPolicy;
use Usamamuneerchaudhary\Commentify\Providers\CommentifyServiceProvider as BaseCommentifyServiceProvider;

class CommentifyServiceProvider extends BaseCommentifyServiceProvider
{
    /**
     * @return void
     */
    public function register(): void
    {
        $this->app->bind(CommentPolicy::class, function ($app) {
            return new CommentPolicy;
        });

        Gate::policy(CommentifyComment::class, CommentPolicy::class);

        $this->app->register(MarkdownServiceProvider::class);
    }

    /**
     * @return void
     */
    public function boot(): void
    {
        parent::boot();

        Livewire::component('comment', Comment::class);
        Livewire::component('comments', Comments::class);
        Livewire::component('like', Like::class);
    }
}
