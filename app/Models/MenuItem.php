<?php

namespace App\Models;

use Quant\Core\Attribute\Getter;
use Quant\Core\Attribute\Setter;
use Quant\Core\Trait\AccessorTrait;

class MenuItem
{
    use AccessorTrait;

    /**
     *  Item title
     *  @var string
     */
    #[Setter] #[Getter]
    private $title;

    /**
     * Item icon
     * @var string
     */
    #[Setter] #[Getter]
    private $icon;

    /**
     * Item url
     * @var string
     */
    #[Setter] #[Getter]
    private $url;

    /**
     * Items children
     *  @var array<MenuItem>
     */
    #[Setter] #[Getter]
    private $children = [];

    /**
     * Item has roles
     * @var array<string>
     */
    #[Setter] #[Getter]
    private $hasRoles = [];

    /**
     * Item has'n roles
     * @var array<string>
     */
    #[Setter] #[Getter]
    private $hasnRoles = [];
}
