<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Spatie\Permission\Models\Role as SpatieRole;

class Role extends SpatieRole
{
    public function team(): BelongsTo
    {
        return $this->belongsTo(Team::class);
    }

    /**
     * Scope the model query to certain permissions only.
     *
     * @param  string|int|array|Permission|Collection|\BackedEnum  $permissions
     * @param  bool  $without
     */
    public function scopeExactPermission(Builder $query, $permissions): Builder
    {
        $permissions = $this->convertToPermissionModels($permissions);
        $permissionKey = (new ($this->getPermissionClass())())->getKeyName();

        $resultQuery = $query->has('permissions', count($permissions))
            ->whereHas(
                'permissions',
                fn ($query) => $query->distinct()->whereIn(config('permission.table_names.permissions') . ".$permissionKey", \array_column($permissions, $permissionKey)),
                '=',
                count($permissions)
            );

        return $resultQuery;
    }

    /**
    * Scope the model query to certain user(and team) only.
    *
    * @param \Illuminate\Database\Eloquent\Builder $query
    * @param int|string|\Illuminate\Database\Eloquent\Model $userId
    * @param null|int|string|\Illuminate\Database\Eloquent\Model $teamId
    */
    public function scopeUser(Builder $query, $userId, $teamId = null): Builder
    {
        if ($userId instanceof Model) {
            $userId = $userId->getKey();
        }

        if ($teamId instanceof Model) {
            $teamId = $teamId->getKey();
        }

        $teams = config('permission.teams');
        $columnNames = config('permission.column_names');
        $teamsKey = $columnNames['team_foreign_key'];

        if ($teams) {
            $teamId = $teamId ?: getPermissionsTeamId();

            $query->where(function ($query) use ($teamId, $teamsKey) {
                $query->whereNull($teamsKey)
                    ->orWhere($teamsKey, $teamId);
            });
        }

        return $query->whereHas('users', function (Builder $query) use ($userId, $teamId, $teams, $teamsKey) {
            $query->where(config('permission.table_names.model_has_roles') . '.' . config('permission.column_names.model_morph_key'), $userId);

            if ($teams) {
                $query->where(config('permission.table_names.model_has_roles') . '.' . $teamsKey, $teamId);
            }
        });
    }
}
