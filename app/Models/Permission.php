<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Spatie\Permission\Models\Permission as SpatiePermission;
use Spatie\Permission\PermissionRegistrar;

class Permission extends SpatiePermission
{
    /**
     * Scope the model query to certain user(and team) only.
     *
     * @param \Illuminate\Database\Eloquent\Builder $query
     * @param int|string|\Illuminate\Database\Eloquent\Model $userId
     * @param null|int|string|\Illuminate\Database\Eloquent\Model $teamId
     */
    public function scopeUser(Builder $query, $userId, $teamId = null): Builder
    {
        if ($userId instanceof Model) {
            $userId = $userId->getKey();
        }

        if ($teamId instanceof Model) {
            $teamId = $teamId->getKey();
        }

        $teams = config('permission.teams');
        $columnNames = config('permission.column_names');
        $teamsKey = $columnNames['team_foreign_key'];

        return $query->whereHas('users', function ($query) use ($userId, $teamId, $teams, $teamsKey) {
            $query->where(config('permission.table_names.model_has_permissions') . '.' . config('permission.column_names.model_morph_key'), $userId);

            if ($teams) {
                $query->where(config('permission.table_names.model_has_permissions') . '.' . $teamsKey, $teamId ?: getPermissionsTeamId());
            }
        });
    }
}
