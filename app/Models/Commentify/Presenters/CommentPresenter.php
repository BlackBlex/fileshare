<?php

namespace App\Models\Commentify\Presenters;

use Usamamuneerchaudhary\Commentify\Models\Presenters\CommentPresenter as BaseCommentPresenter;
use Usamamuneerchaudhary\Commentify\Models\User;

class CommentPresenter extends BaseCommentPresenter
{
    /**
     * @param $text
     * @return array|string
     */
    public function replaceUserMentions($text): array|string
    {
        preg_match_all('/@([A-Za-z0-9_]+)/', $text, $matches);
        $usernames = $matches[1];
        $replacements = [];

        foreach ($usernames as $username) {
            $user = User::where('name', str_replace('_', ' ', $username))->first();

            if ($user) {
                $userRoutePrefix = config('commentify.users_route_prefix', 'users');

                $replacements['@' . $username] = '<a href="/' . $userRoutePrefix . '/' . $username . '">@' . $username .
                    '</a>';
            } else {
                $replacements['@' . $username] = '@' . $username;
            }
        }

        return str_replace(array_keys($replacements), array_values($replacements), $text);
    }
}
