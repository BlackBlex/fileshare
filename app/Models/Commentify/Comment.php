<?php

namespace App\Models\Commentify;

use App\Models\Commentify\Presenters\CommentPresenter;
use App\Models\User;
use Database\Factories\Commentify\CommentFactory;
use Usamamuneerchaudhary\Commentify\Models\Comment as BaseComment;

class Comment extends BaseComment
{
    /**
     * @return CommentPresenter
     */
    public function presenterCustom(): CommentPresenter
    {
        return new CommentPresenter($this);
    }

    /**
     * @return BelongsTo
     */
    public function user(): \Illuminate\Database\Eloquent\Relations\BelongsTo
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    /**
     * @return HasMany
     */
    public function children(): \Illuminate\Database\Eloquent\Relations\HasMany
    {
        return $this->hasMany(Comment::class, 'parent_id')->oldest();
    }

    /**
     * @return CommentFactory
     */
    protected static function newFactory(): CommentFactory
    {
        return CommentFactory::new();
    }
}
