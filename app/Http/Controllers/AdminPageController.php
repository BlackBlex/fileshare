<?php

namespace App\Http\Controllers;

use App\Models\Commentify\Comment;
use App\Models\File;
use App\Models\User;
use Illuminate\Http\Request;

class AdminPageController extends Controller
{
    public function index()
    {
        return redirect()->route('admin_dashboard');
    }

    public function dashboard()
    {
        $fileCount = File::count();
        $fileDownloads = File::sum('downloads');
        $commentCount = Comment::count();
        $userCount = User::count();

        return view('admin.dashboard', compact('fileCount', 'fileDownloads', 'commentCount', 'userCount'));
    }

    public function permissions()
    {
        return view('admin.permissions');
    }

    public function companies()
    {
        return view('admin.companies');
    }

    public function users()
    {
        return view('admin.users');
    }
}
