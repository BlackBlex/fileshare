<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class UserPageController extends Controller
{
    public function index()
    {
        if (!Auth::check()) {
            return redirect()->route('login');
        }

        /** @var \App\Models\User::class */
        $user = Auth::user();

        setPermissionsTeamId($user->current_team_id);

        if ($user->hasAnyRole(['super-admin', 'admin'])) {
            return redirect()->route('admin.dashboard');
        } elseif ($user->hasAnyRole(['editor', 'member'])) {
            return redirect()->route('dashboard');
        }
    }

    public function dashboard()
    {
        return view('user.dashboard');
    }
}
