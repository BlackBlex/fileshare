<?php

namespace App\Http\Responses;

use Illuminate\Support\Facades\Auth;
use Laravel\Fortify\Contracts\LoginResponse as LoginResponseContract;

class LoginResponse implements LoginResponseContract
{
    /**
     * @param  $request
     * @return mixed
     */
    public function toResponse($request)
    {
        /** @var string */
        $home = '/dashboard';
        /** @var \App\Models\User::class */
        $user = Auth::user();

        setPermissionsTeamId($user->current_team_id);

        if ($user->hasAnyRole(['super-admin', 'admin'])) {
            $home = '/admin/dashboard';
        }

        return redirect()->intended($home);
    }
}
