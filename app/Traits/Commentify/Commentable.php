<?php

namespace App\Traits\Commentify;

use App\Models\Commentify\Comment;
use Illuminate\Database\Eloquent\Relations\MorphMany;

trait Commentable
{

    /**
     * @return MorphMany
     */
    public function comments(): \Illuminate\Database\Eloquent\Relations\MorphMany
    {
        return $this->morphMany(Comment::class, 'commentable');
    }
}
