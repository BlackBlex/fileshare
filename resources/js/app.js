import './bootstrap';

import { livewire_hot_reload } from 'virtual:livewire-hot-reload'

livewire_hot_reload();

document.addEventListener('alpine:init', () => {
    Alpine.store('showMenu', {
        visible: Alpine.$persist(true).as('showMenu'),

        toggle() {
            if (this.visible === true) {
                this.visible = false;
            } else {
                this.visible = true;
            }
        }
    });
});
