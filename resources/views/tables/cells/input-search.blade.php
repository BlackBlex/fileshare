<div class="join shadow-sm">
    <input class="input join-item input-bordered block w-full" type="text"
        wire:model.live.debounce="columnSearch.{{ $field }}" placeholder="Search {{ ucfirst($field) }}" />

    @if (isset($columnSearch[$field]) && strlen($columnSearch[$field]))
        <x-button class="join-item" wire:click="$set('columnSearch.{{ $field }}', null)">
            @svg('forkawesome-plus', 'h-4 w-4 rotate-45')
        </x-button>
    @endif
</div>
