@props(['title'])
<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ $title ?? 'Home' }} :: {{ config('app.name', 'Laravel') }}</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet" />

    <!-- Scripts -->
    @vite(['resources/css/app.css', 'resources/js/app.js'])

    <!-- Styles -->
    @livewireStyles
</head>

<body class="font-sans antialiased">
    <x-banner />

    <div class="flex min-h-screen flex-col bg-gray-100 dark:bg-gray-900">
        <header class="sticky top-0 z-30 shadow-md">
            @livewire('header-menu')
        </header>

        <!-- Page Content -->
        <main class="flex flex-1 items-stretch justify-stretch">
            @livewire('nav-menu')

            <div class="max-h-[calc(100vh-3.1rem)] overflow-x-hidden overscroll-contain bg-gray-300 dark:bg-gray-500"
                x-data :class="{ 'w-full': !$store.showMenu.visible, 'md:w-5/6': $store.showMenu.visible }">
                <!-- Page Heading -->
                @if (isset($header))
                    <div class="flex h-14 items-center bg-white px-4 align-middle shadow dark:bg-gray-800">
                        {{ $header }}
                    </div>
                @endif
                {{ $slot }}
            </div>
        </main>
    </div>

    @stack('modals')

    @livewireScripts
</body>

</html>
