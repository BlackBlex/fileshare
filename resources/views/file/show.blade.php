<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 dark:text-gray-200 leading-tight">
            {{ $file->filename }}'s {{ __(' details') }}
        </h2>
    </x-slot>

    <div>
        <div class="max-w-7xl mx-auto py-10 sm:px-6 lg:px-8">
            @livewire('file.detail', ['file' => $file])

            @livewire('comments', ['model' => $file])
        </div>
    </div>
</x-app-layout>
