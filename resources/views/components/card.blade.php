@props(['width_sm' => 'w-1/2', 'padding_x' => 'px-2', 'padding_b' => 'pb-2', 'width' => 'md:w-1/4', 'icon_padding' => 'p-4', 'color' => '', 'icon' => '', 'icon_class' => 'w-10', 'text_one' => '', 'text_two' => ''])

@php
    switch ($color) {
        case 'slate':
            $backgroundIcon = 'bg-slate-700';
            $foregroundIcon = 'text-slate-500';
            $backgroundCard = 'bg-slate-500';
            $foregroundCard = 'text-slate-200';
            break;
        case 'green':
            $backgroundIcon = 'bg-green-700';
            $foregroundIcon = 'text-green-500';
            $backgroundCard = 'bg-green-500';
            $foregroundCard = 'text-green-200';
            break;
        case 'orange':
            $backgroundIcon = 'bg-orange-700';
            $foregroundIcon = 'text-orange-500';
            $backgroundCard = 'bg-orange-500';
            $foregroundCard = 'text-orange-200';
            break;
        case 'blue':
        default:
            $backgroundIcon = 'bg-blue-700';
            $foregroundIcon = 'text-blue-500';
            $backgroundCard = 'bg-blue-500';
            $foregroundCard = 'text-blue-200';
            break;
    }
@endphp

<div class="flex {{ $width_sm }} {{ $padding_x }} {{ $padding_b }} md:pb-0 {{ $width }}">
    <div class="flex items-center justify-center {{ $icon_padding }} rounded-tl rounded-bl {{ $backgroundIcon }}">
        @svg($icon, $icon_class . ' ' . $foregroundIcon)
    </div>
    <div class="w-full pt-4 pb-4 pl-8 pr-4 rounded-tr rounded-br {{ $backgroundCard }}">
        <p class="text-xl font-semibold {{ $foregroundCard }}">{{ $text_one }}</p>
        <p class="text-4xl font-black text-white">{{ $text_two }}</p>
    </div>
</div>
