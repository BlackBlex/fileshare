@props(['active', 'icon', 'title'])

@php
    $commonClass = 'flex justify-center py-[0.5rem] text-[0.85rem] hover:bg-slate-300 hover:text-inherit hover:outline-none focus:bg-slate-200 focus:text-inherit focus:outline-none dark:hover:bg-slate-800 dark:focus:bg-slate-800 leading-5 font-medium hover:text-gray-700 dark:hover:text-gray-400 relative';
    
    $activeClass = 'bg-gray-500 dark:bg-gray-900 text-gray-800 dark:text-gray-200 focus:outline-none focus:bg-black/20 transition duration-150 ease-in-out';
    $inactiveClass = 'border-transparent text-gray-600 dark:text-gray-300 focus:outline-none focus:text-gray-700 dark:focus:text-gray-300 focus:border-gray-300 dark:focus:border-gray-700 transition duration-150 ease-in-out';
    
    $classes = $active ?? false ? $activeClass : $inactiveClass;
@endphp

<a {{ $attributes->merge(['class' => $classes . ' ' . $commonClass]) }} x-data="{ over: false }" @mouseleave="over = false"
    @mouseover="over = true">
    <span class="[&>svg]:h-5 [&>svg]:w-5 [&>svg]:text-gray-400 dark:[&>svg]:text-gray-300"
        :class="{ 'ml-3': $store.showMenu.visible, 'ml-1': !$store.showMenu.visible }">
        @svg($icon)</span>
    <span class="ml-4 flex-1" x-cloak x-show="$store.showMenu.visible">{{ $title }}</span>
    <span class="fixed left-16 z-30 -mt-2 w-[12%] flex-1 bg-slate-300 px-4 py-[0.5rem] dark:bg-slate-800" x-cloak
        x-show="over && !$store.showMenu.visible" x-transition.duration.125ms
        @mouseleave="over = false">{{ $title }}</span>
</a>
