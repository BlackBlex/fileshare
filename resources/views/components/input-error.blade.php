@props(['for'])

@error($for)
    <label {{ $attributes->merge(['class' => 'label text-sm']) }}><span
            class="label-text-alt text-error">{{ $message }}</span></label>
@enderror
