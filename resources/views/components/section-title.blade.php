<div class="flex justify-between md:col-span-1">
    <div class="py-4 pl-4">
        @if (isset($title))
            <h3 class="text-lg font-medium text-gray-900 dark:text-gray-100">{{ $title }}</h3>
        @endif
        <p class="text-md mt-1 text-gray-600 dark:text-gray-400">
            {{ $description }}
        </p>
    </div>

    <div class="px-4 sm:px-0">
        {{ $aside ?? '' }}
    </div>
</div>
