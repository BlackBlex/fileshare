<!-- //TODO: bug when click, state bg focus -->
<div class="-mr-2 mb-4 flex pl-2 pr-4">
    <button
        class="inline-flex flex-1 items-center justify-center rounded-md bg-gray-300 p-2 text-gray-400 transition duration-150 ease-in-out hover:bg-gray-400 hover:text-gray-500 focus:bg-gray-100 focus:text-gray-500 focus:outline-none dark:bg-gray-800 dark:text-gray-500 dark:hover:bg-gray-900 dark:hover:text-gray-400 dark:focus:bg-gray-900 dark:focus:text-gray-400"
        @click="$store.showMenu.toggle()">
        <svg class="h-5 w-5" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1792 1792" fill="currentColor">
            {{-- Left --}}
            <path x-cloak :class="{ 'hidden': !$store.showMenu.visible }"
                d="M1536 896v128c0 68-45 128-117 128H715l293 294c24 23 38 56 38 90s-14 67-38 90l-75 76c-23 23-56 37-90 37s-67-14-91-37l-651-652c-23-23-37-56-37-90s14-67 37-91l651-650c24-24 57-38 91-38s66 14 90 38l75 74c24 24 38 57 38 91s-14 67-38 91L715 768h704c72 0 117 60 117 128z">
            </path>
            {{-- Right --}}
            <path x-cloak :class="{ 'hidden': $store.showMenu.visible }"
                d="M1472 960c0 34-13 67-37 91l-651 651c-24 23-57 37-91 37s-66-14-90-37l-75-75c-24-24-38-57-38-91s14-67 38-91l293-293H117c-72 0-117-60-117-128V896c0-68 45-128 117-128h704L528 474c-24-23-38-56-38-90s14-67 38-90l75-75c24-24 56-38 90-38s67 14 91 38l651 651c24 23 37 56 37 90z">
            </path>
        </svg>

        <span class="ml-2" x-cloak x-show="$store.showMenu.visible">{{ __('Collapse menu') }}</span>
    </button>
</div>
