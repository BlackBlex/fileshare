<x-app-layout :title="'Permissions'">
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800 dark:text-gray-200">
            {{ __('Permissions') }}
        </h2>
    </x-slot>

    <div class="px-4 py-3">
        <div class="mx-auto max-w-full sm:px-3 lg:px-4">
            <div class="overflow-hidden px-2 pt-3 shadow-xl sm:rounded-lg">
                <div class="pb-6" x-data="{
                    activeTab: 1,
                    activeClass: 'border-gray-100 dark:border-gray-700 border-l-2 border-t-4 border-r-2 text-gray-800 dark:text-gray-200 border-t-indigo-400 dark:border-t-indigo-600 focus:outline-none focus:border-t-indigo-700 transition duration-150 ease-in-out',
                    inactiveClass: 'text-gray-600 dark:text-gray-400 hover:text-gray-500'
                }">
                    <ul class="flex justify-center border-b-2 border-gray-100 dark:border-gray-700">
                        <li :class="{ '-mb-0.5': activeTab === 1 }">
                            <a class="inline-block bg-white px-4 py-2 text-xl font-semibold dark:bg-gray-800"
                                href="#" x-on:click="activeTab = 1"
                                :class="activeTab === 1 ? activeClass : inactiveClass">Roles</a>
                        </li>
                        <li :class="{ '-mb-0.5': activeTab === 2 }">
                            <a class="inline-block bg-white px-4 py-2 text-xl font-semibold dark:bg-gray-800"
                                href="#" x-on:click="activeTab = 2"
                                :class="activeTab === 2 ? activeClass : inactiveClass">Permissions</a>
                        </li>
                    </ul>
                    <div class="w-full bg-white p-6 text-gray-800 dark:bg-gray-800 dark:text-gray-200">
                        <div x-show="activeTab === 1">
                            @livewire('role.table')
                        </div>
                        <div x-show="activeTab === 2">
                            @livewire('permission.table')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
