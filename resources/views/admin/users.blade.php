<x-app-layout :title="'Users'">
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800 dark:text-gray-200">
            {{ __('Users') }}
        </h2>

        <a class="btn ml-auto" href="{{ route('admin.users.create') }}">
            {{ __('New') }}
        </a>
    </x-slot>

    <div class="px-4 py-3">
        <div class="mx-auto max-w-full sm:px-3 lg:px-4">
            <div class="overflow-hidden bg-white px-2 py-3 shadow-xl dark:bg-gray-800 sm:rounded-lg">
                @livewire('user.table')
            </div>
        </div>
    </div>
</x-app-layout>
