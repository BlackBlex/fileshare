<x-app-layout :title="'Companies'">
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800 dark:text-gray-200">
            {{ __('Companies') }}
        </h2>
    </x-slot>

    <div class="px-4 py-3">
        <div class="mx-auto max-w-full sm:px-3 lg:px-4">
            <div class="overflow-hidden bg-white px-2 py-3 shadow-xl dark:bg-gray-800 sm:rounded-lg">
                @livewire('company.table')
            </div>
        </div>
    </div>
</x-app-layout>
