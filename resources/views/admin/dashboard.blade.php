<x-app-layout :title="'Dashboard'">
    <x-slot name="header">
        <h2 class="text-xl font-semibold leading-tight text-gray-800 dark:text-gray-200">
            {{ __('Dashboard admin') }}
        </h2>
    </x-slot>

    <div class="px-4 py-3">
        <div class="mb-6 pt-1">
            <div class="-mx-2 flex flex-wrap">
                @include('components.card', [
                    'icon' => 'forkawesome-file-o',
                    'color' => 'slate',
                    'text_one' => __('Files'),
                    'text_two' => $fileCount,
                ])
                @include('components.card', [
                    'icon' => 'forkawesome-download',
                    'color' => 'green',
                    'text_one' => __('Downloads'),
                    'text_two' => $fileDownloads,
                ])
                @include('components.card', [
                    'icon' => 'forkawesome-commenting-o',
                    'color' => 'orange',
                    'text_one' => __('Comments'),
                    'text_two' => $commentCount,
                ])
                @include('components.card', [
                    'icon' => 'forkawesome-user',
                    'icon_class' => 'w-10 ml-2',
                    'icon_padding' => 'py-4 pl-3 pr-3',
                    'color' => 'blue',
                    'text_one' => __('Users'),
                    'text_two' => $userCount,
                ])
            </div>
        </div>
        <div class="mx-auto max-w-full sm:px-3 lg:px-4">
            <div class="px-2">
                <div class="-mx-2 flex">
                    <div class="w-1/2 px-2">
                        <div class="bg-white shadow-xl dark:bg-gray-800">
                            <p class="mb-1 mt-2 px-4 text-center text-3xl font-black text-gray-900 dark:text-gray-300">
                                {{ __('Latest uploaded files') }}</p>
                            @livewire('file.simpletable')
                        </div>
                    </div>
                    <div class="w-1/2 px-2">
                        <div class="bg-white shadow-xl dark:bg-gray-800">
                            <p class="mb-1 mt-2 px-4 text-center text-3xl font-black text-gray-900 dark:text-gray-300">
                                {{ __('Latest users') }}</p>
                            @livewire('user.simpletable')
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</x-app-layout>
