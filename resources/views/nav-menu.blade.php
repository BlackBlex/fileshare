<nav class="max-h-[calc(100vh-3.1rem)] overflow-visible overflow-x-hidden overscroll-contain rounded-r bg-gray-100 py-2 dark:bg-gray-700"
    x-cloak :class="{ 'w-20': !$store.showMenu.visible, 'md:w-1/6': $store.showMenu.visible }">
    <x-button-show-menu />
    <ul class="w-full">
        @foreach ($menu as $menuItem)
            <li>
                @if (empty($menuItem->getChildren()))
                    @if (!empty($menuItem->getHasRoles()))
                        @hasanyrole($menuItem->getHasRoles())
                            <x-nav-menu-link href="{{ route($menuItem->getUrl()) }}" :active="request()->routeIs($menuItem->getUrl() . '*')" :icon="$menuItem->getIcon()"
                                :title="__($menuItem->getTitle())" />
                        @endhasrole
                    @else
                        @hasanyrole($menuItem->getHasnRoles())
                        @else
                            <x-nav-menu-link href="{{ route($menuItem->getUrl()) }}" :active="request()->routeIs($menuItem->getUrl() . '*')" :icon="$menuItem->getIcon()"
                                :title="__($menuItem->getTitle())" />
                        @endhasrole
                    @endif
                @else
                    <span class="px-6 py-4 text-[0.6rem] font-bold uppercase text-gray-600 dark:text-gray-400" x-cloak
                        x-show="$store.showMenu.visible">{{ __($menuItem->getTitle()) }}</span>
                    <hr class="my-2 text-gray-600 dark:text-gray-400" x-cloak x-show="!$store.showMenu.visible"
                        x-transition.duration.50ms />
                    @foreach ($menuItem->getChildren() as $menuItemChild)
            <li>
                @if (!empty($menuItemChild->getHasRoles()))
                    @hasanyrole($menuItemChild->getHasRoles())
                        <x-nav-menu-link href="{{ route($menuItemChild->getUrl()) }}" :active="request()->routeIs($menuItemChild->getUrl() . '*')" :icon="$menuItemChild->getIcon()"
                            :title="__($menuItemChild->getTitle())" />
                    @endhasrole
                @else
                    @hasanyrole($menuItemChild->getHasnRoles())
                    @else
                        <x-nav-menu-link href="{{ route($menuItemChild->getUrl()) }}" :active="request()->routeIs($menuItemChild->getUrl() . '*')" :icon="$menuItemChild->getIcon()"
                            :title="__($menuItemChild->getTitle())" />
                    @endhasrole
                @endif
            </li>
        @endforeach
        @endif
        </li>
        @endforeach
    </ul>
</nav>
